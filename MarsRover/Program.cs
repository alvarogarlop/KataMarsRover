﻿using MarsRover.Commands;
using MarsRover.Contracts;
using MarsRover.Models;
using MarsRover.Models.Orientations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MarsRover
{
    class Program
    {
        static void Main(string[] args)
        {
            var allCommands = CreateCommands();
            var map = new Map(5, 3);
            var robotPosition = new Position{X = 1, Y = 1};
            var orientation = new East();
            var robot = new Robot('R', orientation, map, robotPosition);

            while (true)
            {
                Console.WriteLine(map.Print(robot));
                Console.WriteLine($"Orientation: {robot.Orientation}");

                Console.WriteLine("Insert Command");
                var stringCommands = Console.ReadLine()?.ToUpper();
                foreach (var stringCommand in stringCommands)
                {
                    var command = GetCommand(allCommands, stringCommand);
                    command.Execute(robot);
                }
            }
        }

        private static ICommand GetCommand(List<ICommand> allCommands, char stringCommand)
        {
            return allCommands.FirstOrDefault(x => x.CommandName.Equals(stringCommand)) ?? new NullCommand();
        }

        private static List<ICommand> CreateCommands()
        {
            return
                new List<ICommand>
                {
                    new MoveForward(),
                    new MoveBackward(),
                    new TurnLeft(),
                    new TurnRight(),
                    new MoveNorthEast()
                };
        }
    }
}
