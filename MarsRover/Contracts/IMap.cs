﻿using MarsRover.Models;

namespace MarsRover.Contracts
{
    public interface IMap
    {
        bool IsValid(Position position);
    }
}