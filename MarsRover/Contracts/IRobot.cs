﻿namespace MarsRover.Contracts
{
    public interface IRobot
    {
        void TurnRight();
        void TurnLeft();
        void MoveForward();
        void MoveBackward();
        string Orientation { get; }
    }
}
