﻿namespace MarsRover.Contracts
{
    public interface ICommand
    {
        char CommandName { get; }
        void Execute(IRobot robot);
    }
}
