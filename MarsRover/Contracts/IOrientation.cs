﻿using MarsRover.Models;

namespace MarsRover.Contracts
{
    public interface IOrientation
    {
        IOrientation TurnLeft();
        IOrientation TurnRight();
        void MoveForward(Position from, IMap map);
        void MoveBackward(Position from, IMap map);
    }
}
