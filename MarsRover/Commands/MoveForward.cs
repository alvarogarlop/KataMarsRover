﻿using MarsRover.Contracts;

namespace MarsRover.Commands
{
    public class MoveForward
        : ICommand
    {
        public char CommandName => 'F';

        public void Execute(IRobot robot)
        {
            robot.MoveForward();
        }
    }
}
