﻿using MarsRover.Contracts;

namespace MarsRover.Commands
{
    public class TurnRight
        : ICommand
    {
        public char CommandName => 'R';

        public void Execute(IRobot robot)
        {
            robot.TurnRight();
        }
    }
}