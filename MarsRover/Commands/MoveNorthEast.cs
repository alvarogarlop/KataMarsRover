﻿using MarsRover.Contracts;

namespace MarsRover.Commands
{
    public class MoveNorthEast
        : ICommand
    {
        public char CommandName => 'P';
        public void Execute(IRobot robot)
        {
            robot.MoveForward();
            robot.TurnRight();
            robot.MoveForward();
        }
    }
}
