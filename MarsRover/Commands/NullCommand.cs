﻿using MarsRover.Contracts;

namespace MarsRover.Commands
{
    public class NullCommand
        : ICommand
    {
        public char CommandName => ' ';

        public void Execute(IRobot robot)
        {
        }
    }
}
