﻿using MarsRover.Contracts;

namespace MarsRover.Commands
{
    public class MoveBackward
        : ICommand
    {
        public char CommandName => 'B';

        public void Execute(IRobot robot)
        {
            robot.MoveBackward();
        }
    }
}
