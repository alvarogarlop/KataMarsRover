﻿using MarsRover.Contracts;

namespace MarsRover.Commands
{
    public class TurnLeft
        : ICommand
    {
        public char CommandName => 'L';

        public void Execute(IRobot robot)
        {
            robot.TurnLeft();
        }
    }
}