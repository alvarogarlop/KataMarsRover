﻿using MarsRover.Contracts;
using System.Text;

namespace MarsRover.Models
{
    public class Map
        : IMap
    {
        private readonly int _width;
        private readonly int _height;

        public Map(int width, int height)
        {
            _width = width;
            _height = height;
        }

        public string Print(Robot robot)
        {
            var map = new StringBuilder();
            for (var i = 0; i < _height; i++)
            {
                for (var j = 0; j < _width; j++)
                {
                    var isPositionOfRobot = robot.Position.X.Equals(j) && robot.Position.Y.Equals(i);
                    if (isPositionOfRobot)
                    {
                        map.Append(robot.Name);
                    }
                    else
                    {
                        map.Append("X");
                    }
                }

                if (i < _height - 1)
                {
                    map.AppendLine();
                }
            }

            return map.ToString();
        }

        public bool IsValid(Position position)
        {
            var xIsValid = position.X < _width && position.X >= 0;
            var yIsValid = position.Y < _height && position.Y >= 0;

            return xIsValid && yIsValid;
        }
    }
}
