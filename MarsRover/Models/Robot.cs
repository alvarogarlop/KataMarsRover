﻿using MarsRover.Contracts;

namespace MarsRover.Models
{
    public class Robot
        : IRobot
    {
        private IOrientation _orientation;
        private readonly IMap _map;
        public Position Position { get; }
        public char Name { get; }
        public string Orientation => _orientation.ToString();

        public Robot(char name, IOrientation orientation, IMap map, Position position)
        {
            Name = name;
            _orientation = orientation;
            _map = map;
            Position = position;
        }

        public void TurnRight()
        {
            _orientation = _orientation.TurnRight();
        }

        public void TurnLeft()
        {
            _orientation = _orientation.TurnLeft();
        }

        public void MoveForward()
        {
            _orientation.MoveForward(Position, _map);
        }

        public void MoveBackward()
        {
            _orientation.MoveBackward(Position, _map);
        }
    }
}