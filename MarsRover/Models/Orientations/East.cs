﻿using MarsRover.Contracts;

namespace MarsRover.Models.Orientations
{
    public class East
        : IOrientation
    {
        public IOrientation TurnLeft()
        {
            return new North();
        }

        public IOrientation TurnRight()
        {
            return new South();
        }

        public void MoveForward(Position @from, IMap map)
        {
            var finalPosition = new Position{X = from.X + 1, Y = from.Y};
            if (map.IsValid(finalPosition))
            {
                from.X++;
            }
        }

        public void MoveBackward(Position @from, IMap map)
        {
            var finalPosition = new Position{X = from.X - 1, Y = from.Y};
            if (map.IsValid(finalPosition))
            {
                from.X--;
            }
        }

        public override string ToString()
        {
            return "East";
        }
    }
}
