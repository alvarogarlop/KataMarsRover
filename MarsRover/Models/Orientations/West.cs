﻿using MarsRover.Contracts;

namespace MarsRover.Models.Orientations
{
    public class West
        : IOrientation
    {
        public IOrientation TurnLeft()
        {
            return new South();
        }

        public IOrientation TurnRight()
        {
            return new North();
        }

        public void MoveForward(Position @from, IMap map)
        {
            var finalPosition = new Position{X = from.X - 1, Y = from.Y};
            if (map.IsValid(finalPosition))
            {
                from.X--;
            }
        }

        public void MoveBackward(Position @from, IMap map)
        {
            var finalPosition = new Position{X = from.X + 1, Y = from.Y};
            if (map.IsValid(finalPosition))
            {
                from.X++;
            }
        }

        public override string ToString()
        {
            return "West";
        }
    }
}
