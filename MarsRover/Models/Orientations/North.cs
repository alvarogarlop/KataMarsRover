﻿using MarsRover.Contracts;

namespace MarsRover.Models.Orientations
{
    public class North
        : IOrientation
    {
        public IOrientation TurnLeft()
        {
            return new West();
        }

        public IOrientation TurnRight()
        {
            return new East();
        }

        public void MoveForward(Position @from, IMap map)
        {
            var finalPosition = new Position{X = from.X, Y = from.Y - 1};
            if (map.IsValid(finalPosition))
            {
                from.Y--;
            }
        }

        public void MoveBackward(Position @from, IMap map)
        {
            var finalPosition = new Position{X = from.X, Y = from.Y + 1};
            if (map.IsValid(finalPosition))
            {
                from.Y++;
            }
        }

        public override string ToString()
        {
            return "North";
        }
    }
}
