﻿using FluentAssertions;
using MarsRover.Contracts;
using MarsRover.Models;
using Moq;
using Xunit;

namespace MarsRover.Tests.Models
{
    public class RobotTests
    {
        private Robot _sut;
        private readonly Mock<IOrientation> _orientation;
        private Position _position;

        public RobotTests()
        {
            var map = Mock.Of<IMap>();
            _orientation = new Mock<IOrientation>();
            var orientation = _orientation.Object;
            _position = new Position();
            _sut = new Robot('R', orientation, map, _position);
        }

        [Fact]
        public void Given_A_Robot_When_Turning_Right_Then_The_Orientation_Should_Turn_Right()
        {
            _sut.TurnRight();
            _orientation.Verify(x => x.TurnRight());
        }

        [Fact]
        public void Given_A_Robot_When_Turning_Left_Then_The_Orientation_Should_Turn_Left()
        {
            _sut.TurnLeft();
            _orientation.Verify(x => x.TurnLeft());
        }

        [Fact]
        public void Given_A_Map_Of_2_3_And_A_Robot_Located_On_0_1_When_Constructing_The_Robot_Then_It_Shuold_Be_Located_On_0_1()
        {
            var orientation = Mock.Of<IOrientation>();
            var map = Mock.Of<IMap>();
            _position = new Position
            {
                X = 0,
                Y= 1
            };
            _sut = new Robot('R', orientation, map, _position);

            _sut.Position.X.Should().Be(0);
            _sut.Position.Y.Should().Be(1);
        }

        [Fact]
        public void Given_A_Map_Of_2_3_And_A_Robot_Located_On_0_0_And_Pointing_At_East_When_Moving_Forward_Then_The_Position_Should_Be_0_1()
        {
            var orientation = Mock.Of<IOrientation>();
            var map = Mock.Of<IMap>();
            _position = new Position
            {
                X = 0,
                Y = 1
            };
            _sut = new Robot('R', orientation, map, _position);

            _sut.Position.X.Should().Be(0);
            _sut.Position.Y.Should().Be(1);
        }

        //        [Fact]
        //        public void Given_A_Map_Of_2_3_And_A_Robot_Located_On_1_1_When_Displaying_The_Map_Then_It_Should_Display_The_Robot_On_Position_1_1()
        //        {
        //            var orientation = Mock.Of<IOrientation>();
        //            var mapMock = new Mock<IMap>();
        //            var stringMap = @"XX
        //XX
        //XX";

        //            mapMock
        //                .Setup(x => x.ToString())
        //                .Returns(stringMap);

        //            var map = mapMock.Object;
        //            _sut = new Robot(orientation, map);

        //            var mapPrinted = _sut.ScanMap();
        //            var expectedMap = @"XX
        //XM
        //XX";

        //            mapPrinted.Should().Be(expectedMap);
        //        }
    }
}
