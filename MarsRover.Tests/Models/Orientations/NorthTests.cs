﻿using FluentAssertions;
using MarsRover.Contracts;
using MarsRover.Models;
using MarsRover.Models.Orientations;
using Moq;
using Xunit;

namespace MarsRover.Tests.Models.Orientations
{
    public class NorthTests
    {
        private readonly North _sut;
        private readonly Mock<IMap> _mapMock;

        public NorthTests()
        {
            _sut = new North();
            _mapMock = new Mock<IMap>();
        }

        [Fact]
        public void Given_A_North_Orientation_When_Turning_Left_Then_IT_Should_REturn_A_West_Orientation()
        {
            var result = _sut.TurnLeft();

            result.Should().BeAssignableTo<West>();
        }

        [Fact]
        public void Given_A_North_Orientation_When_Turning_Right_Then_IT_Should_Return_A_East_Orientation()
        {
            var result = _sut.TurnRight();

            result.Should().BeAssignableTo<East>();
        }

        [Fact]
        public void Given_A_North_Orientation_When_Moving_Forward_From_0_1_Then_It_Should_Return_0_0()
        {
            var position = new Position { X = 0, Y = 1 };
            _mapMock
                .Setup(x => x.IsValid(It.IsAny<Position>()))
                .Returns(true);
            var map = _mapMock.Object;

            _sut.MoveForward(position, map);

            position.X.Should().Be(0);
            position.Y.Should().Be(0);
        }

        [Fact]
        public void Given_A_North_Orientation_When_Moving_Forward_To_A_None_Valid_Position_Then_It_Should_Not_Move()
        {
            var position = new Position { X = 0, Y = 3 };
            _mapMock
                .Setup(x => x.IsValid(It.IsAny<Position>()))
                .Returns(false);
            var map = _mapMock.Object;

            _sut.MoveForward(position, map);

            position.X.Should().Be(0);
            position.Y.Should().Be(3);
        }

        [Fact]
        public void Given_A_North_Orientation_When_Moving_Backward_To_A_None_Valid_Position_Then_It_Should_Not_Move()
        {
            var position = new Position { X = 0, Y = 0 };
            _mapMock
                .Setup(x => x.IsValid(It.IsAny<Position>()))
                .Returns(false);
            var map = _mapMock.Object;

            _sut.MoveBackward(position, map);

            position.X.Should().Be(0);
            position.Y.Should().Be(0);
        }
    }
}
