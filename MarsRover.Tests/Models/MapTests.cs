﻿using FluentAssertions;
using MarsRover.Models;
using MarsRover.Tests.Builders;
using Xunit;

namespace MarsRover.Tests.Models
{
    public class MapTests
    {
        private Map _sut;

        [Fact]
        public void Given_A_Map_Of_2_Of_Width_And_3_Of_Height_With_A_Robot_Named_R_On_Position_1_1_When_Displaying_The_Map_Then_It_Should_Display_Expected_Map()
        {
            _sut = new Map(2, 3);
            var robot =
                new RobotBuilder()
                    .WithName('R')
                    .WithPosition(1, 1)
                    .Build();

            var expectedMap = @"XX
XR
XX";
            var result = _sut.Print(robot);

            result.Should().Be(expectedMap);
        }

        [Theory]
        [InlineData(2, 1, 2, 0, false)]
        [InlineData(2, 1, -1, 0, false)]
        [InlineData(1, 2, 0, 2, false)]
        [InlineData(1, 2, 0, -1, false)]
        [InlineData(2, 2, 1, 1, true)]
        public void Given_A_Map_Of_Width_And_Height_And_A_Position_On_X_Y_When_Validating_The_Position_Then_It_Should_Return_Expected(int width, int height, int x, int y, bool expected)
        {
            _sut = new Map(width, height);
            var position = new Position{X=x, Y=y};

            var result = _sut.IsValid(position);
            result.Should().Be(expected);
        }
    }
}