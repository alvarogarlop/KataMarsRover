﻿using MarsRover.Contracts;
using MarsRover.Models;
using Moq;

namespace MarsRover.Tests.Builders
{
    public class RobotBuilder
    {
        private readonly IOrientation _orientation;
        private readonly IMap _map;
        private Position _position;
        private char _name;

        public RobotBuilder()
        {
            _name = 'R';
            _orientation = Mock.Of<IOrientation>();
            _position = new Position();
            _map = Mock.Of<IMap>();
        }

        public RobotBuilder WithName(char name)
        {
            _name = name;
            return this;
        }

        public RobotBuilder WithPosition(int x, int y)
        {
            _position =
                new Position
                {
                    X = x,
                    Y = y
                };
            return this;
        }

        public Robot Build()
        {
            return new Robot(_name, _orientation, _map, _position);
        }
    }
}