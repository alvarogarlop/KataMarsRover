﻿using MarsRover.Commands;
using MarsRover.Contracts;
using Moq;
using Xunit;

namespace MarsRover.Tests.Commands
{
    public class MoveForwardTests
    {
        private readonly MoveForward _sut;

        public MoveForwardTests()
        {
            _sut = new MoveForward();
        }

        [Fact]
        public void Given_A_Robot_When_Executing_A_Move_Forward_Command_Then_The_Robot_Should_Move_Forward()
        {
            var robotMock = new Mock<IRobot>();
            var robot = robotMock.Object;
            _sut.Execute(robot);

            robotMock.Verify(x => x.MoveForward());
        }
    }
}