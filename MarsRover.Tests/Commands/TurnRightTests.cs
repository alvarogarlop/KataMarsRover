﻿using MarsRover.Commands;
using MarsRover.Contracts;
using Moq;
using Xunit;

namespace MarsRover.Tests.Commands
{
    public class TurnRightTests
    {
        private readonly TurnRight _sut;

        public TurnRightTests()
        {
            _sut = new TurnRight();
        }

        [Fact]
        public void Given_A_Robot_When_Executing_A_Turn_Right_Command_Then_The_Robot_Should_Turn_Right()
        {
            var robotMock = new Mock<IRobot>();
            var robot = robotMock.Object;
            _sut.Execute(robot);

            robotMock.Verify(x => x.TurnRight());
        }
    }
}