﻿using MarsRover.Commands;
using MarsRover.Contracts;
using Moq;
using Xunit;

namespace MarsRover.Tests.Commands
{
    public class MoveBackwardTests
    {
        private readonly MoveBackward _sut;

        public MoveBackwardTests()
        {
            _sut = new MoveBackward();
        }

        [Fact]
        public void Given_A_Robot_When_Executing_A_Move_Backward_Command_Then_The_Robot_Should_Move_Backward()
        {
            var robotMock = new Mock<IRobot>();
            var robot = robotMock.Object;
            _sut.Execute(robot);

            robotMock.Verify(x => x.MoveBackward());
        }
    }
}