﻿using MarsRover.Commands;
using MarsRover.Contracts;
using Moq;
using Xunit;

namespace MarsRover.Tests.Commands
{
    public class TurnLeftTests
    {
        private readonly TurnLeft _sut;

        public TurnLeftTests()
        {
            _sut = new TurnLeft();
        }

        [Fact]
        public void Given_A_Robot_When_Executing_A_Turn_Left_Command_Then_The_Robot_Should_Turn_Left()
        {
            var robotMock = new Mock<IRobot>();
            var robot = robotMock.Object;
            _sut.Execute(robot);

            robotMock.Verify(x => x.TurnLeft());
        }
    }
}